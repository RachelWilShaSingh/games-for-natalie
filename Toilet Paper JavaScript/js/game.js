console.log( "game.js" );

bear = {
    x: 0,
    y: 0,
    width: 48,
    height: 48,
    fullWidth: 48,
    fullHeight: 48,
    speed: 5,
    image: null,

    Move: function( dir ) {
        if ( dir == "UP" ) {
            bear.y -= bear.speed;
        }
        else if ( dir == "DOWN" ) {
            bear.y += bear.speed;
        }
        else if ( dir == "LEFT" ) {
            bear.x -= bear.speed;
        }
        else if ( dir == "RIGHT" ) {
            bear.x += bear.speed;
        }
    }
};

gameState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,
    
    keys: {
        UP:         { code: "w", isDown: false }, 
        DOWN:       { code: "s", isDown: false },
        RIGHT:      { code: "d", isDown: false },
        LEFT:       { code: "a", isDown: false },
        SHOOT:      { code: 32, isDown: false },
    },

    objBear : null,

    Init: function( canvas, options ) {
        gameState.canvas = canvas;
        gameState.options = options;
        gameState.isDone = false;

        gameState.images.bear = new Image();
        gameState.images.bear.src = "assets/images/bear.png";

        gameState.objBear = bear;
        gameState.objBear.image = gameState.images.bear;
        gameState.objBear.x = 640/2 - 48/2;
        gameState.objBear.y = 480/2 - 48/2;

        
        UI_TOOLS.CreateButton( { title: "up", words: "W", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 60, y: 10, textX: 10, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "UP" ); } } );
        UI_TOOLS.CreateButton( { title: "left", words: "A", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 10, y: 60, textX: 15, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "LEFT" ); } } );
        UI_TOOLS.CreateButton( { title: "down", words: "S", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 60, y: 60, textX: 15, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "DOWN" ); } } );
        UI_TOOLS.CreateButton( { title: "right", words: "D", color: "#000000", font: "bold 20px Sans-serif",
                             src: "assets/ui/key.png", x: 110, y: 60, textX: 15, textY: 30, width: 100, height: 100, fullWidth: 50, fullHeight: 50,
                             Click: function() { gameState.objBear.Move( "RIGHT" ); } } );
    },

    Clear: function() {
        UI_TOOLS.ClearUI();
    },

    Click: function( ev ) {
        UI_TOOLS.Click( ev );
    },

    KeyPress: function( ev ) {
        if ( ev.key == "Escape" ) {
            main.changeState( "titleState" );
        }
        
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = true;
            }
        } );
    },

    KeyRelease: function( ev ) {
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = false;
            }
        } );
    },

    Update: function() {        
        if ( gameState.keys.UP.isDown ) {
            gameState.objBear.Move( "UP" );
        }
        else if ( gameState.keys.DOWN.isDown ) {
            gameState.objBear.Move( "DOWN" );
        }
        if ( gameState.keys.LEFT.isDown ) {
            gameState.objBear.Move( "LEFT" );
        }
        else if ( gameState.keys.RIGHT.isDown ) {
            gameState.objBear.Move( "RIGHT" );
        }
    },

    Draw: function() {
        // Draw grass 9cc978
        main.canvasWindow.fillStyle = "#9cc978";
        main.canvasWindow.fillRect( 0, 0, main.settings.width, main.settings.height );

        // Draw bear
        main.canvasWindow.drawImage(
            gameState.objBear.image,
            0, 0, gameState.objBear.width, gameState.objBear.height,
            gameState.objBear.x, gameState.objBear.y,
            gameState.objBear.fullWidth, gameState.objBear.fullHeight );
            
        UI_TOOLS.Draw( gameState.canvas );
    },

    ClickPlay: function() {
    }
};
