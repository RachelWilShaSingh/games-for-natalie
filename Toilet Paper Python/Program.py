import pygame, sys
from pygame.locals import *

from states import GameState
from states import TitleState
from Ezha import ConfigManager
from Ezha import FontManager
from Ezha import SoundManager
from Ezha import LanguageManager

class Program( object ):
    
    def __init__( self ):
        self.fpsClock = None
        self.windowSurface = None
        self.screenWidth = 1280
        self.screenHeight = 720
        self.isDone = False
        self.fps = 60
        self.clearColor = pygame.Color( 151, 255, 140 )
        self.states = {}
    
    def Init( self ):
        pygame.init()
        self.fpsClock = pygame.time.Clock()
        self.windowSurface = pygame.display.set_mode( ( self.screenWidth, self.screenHeight ) )
        
        FontManager.Add( "main", "assets/NotoSans-Regular.ttf", 40 )
        FontManager.Add( "win", "assets/NotoSans-Regular.ttf", 60 )
        FontManager.Add( "credit", "assets/NotoSans-Regular.ttf", 20 )


        SoundManager.Add( "title", "assets/Incompetech_SaltyDitty.mp3", True )
        SoundManager.Add( "game", "assets/Incompetech_CyborgNinja.mp3", True )
        SoundManager.Add( "pickup", "assets/pickup.wav", False )
        
        pygame.display.set_caption( "Toilet Paper" )

        
        self.states[ "title" ] = TitleState()
        self.states[ "game" ] = GameState()
        
        self.currentState = self.states[ "title" ]
        self.currentState.Setup( self.screenWidth, self.screenHeight )
        
    def Run( self ):
        while ( self.isDone is False ):
            self.Update()
            self.Draw()

            if ( self.currentState.GotoState() != "" ):
                self.ChangeState( self.currentState.GotoState() )
    
    def Update( self ):
        self.currentState.Update()

    def ChangeState( self, key ):
        self.currentState = self.states[ key ]
        self.currentState.Setup( self.screenWidth, self.screenHeight )
        
    def Draw( self ):
        self.windowSurface.fill( self.clearColor )
        
        self.currentState.Draw( self.windowSurface )
        
        pygame.display.update()
        self.fpsClock.tick( self.fps )
