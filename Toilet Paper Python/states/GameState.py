#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math

import pygame, sys
from pygame.locals import *

from Ezha import BaseState
from Ezha import FontManager
from Ezha import ImageManager
from Ezha import LanguageManager
from Ezha import SoundManager
from Ezha import Button
from Ezha import Image
from Ezha import Label

from PhysObject import PhysObject

class GameState( BaseState ):
    def __init__( self ):
        self.stateName = "gamestate"

    def GotoState( self ):
        return self.gotoState

    def GetDistance( self, rectA, rectB ):
        dx = rectB.x - rectA.x
        dy = rectB.y - rectA.y
        return math.sqrt( dx ** 2 + dy ** 2 )

    def IsBoxCollision( self, rectA, rectB ):
        return (    rectA.x <= rectB.x + rectB.width and
                    rectA.x + rectA.width >= rectB.x and
                    rectA.y <= rectB.y + rectB.height and
                    rectA.y + rectA.height >= rectB.y )
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight


        self.backgroundColor = pygame.Color( 151, 255, 140 )
        self.scoreColor = pygame.Color( 255, 255, 255 )

        self.steps = [ "1. Remove old TP roll", "2. Throw old TP roll away", "3. Put new TP on TP holder", "4. That's not the right direction! Rotate it!" ]
        self.step = 0
        self.jitter = 0
        
        ImageManager.Add( "background", "assets/background.png" )
        ImageManager.Add( "toiletpaper", "assets/fresh-tp.png" )
        ImageManager.Add( "roll", "assets/empty-roll.png" )
        ImageManager.Add( "holder", "assets/tp-holder.png" )
        ImageManager.Add( "shelf", "assets/shelf.png" )
        ImageManager.Add( "hand", "assets/hand.png" )
        ImageManager.Add( "basketf", "assets/basket-front.png" )
        ImageManager.Add( "basketb", "assets/basket-back.png" )

        self.objects = {}

        self.objects["tp"] = PhysObject()
        self.objects["tp"].Setup( ImageManager.Get( "toiletpaper" ), 0, 0, 80, 96 )
        self.objects["tp"].SetPosition( 828, 160 )
        self.objects["tp"].SetMaxAcceleration( 100 )
        self.objects["tp"].SetCanBeMoved( True )

        self.objects["shelf"] = PhysObject()
        self.objects["shelf"].Setup( ImageManager.Get( "shelf" ), 0, 0, 416, 504 )
        self.objects["shelf"].SetPosition( 804, 53 )
        self.objects["shelf"].SetCanBeMoved( False )

        self.objects["tpholder"] = PhysObject()
        self.objects["tpholder"].Setup( ImageManager.Get( "holder" ), 0, 0, 132, 32 )
        self.objects["tpholder"].SetPosition( 156, 384 )

        self.objects["roll"] = PhysObject()
        self.objects["roll"].Setup( ImageManager.Get( "roll" ), 0, 0, 84, 24 )
        self.objects["roll"].SetPosition( 180, 388 )
        self.objects["roll"].SetMaxAcceleration( 100 )
        self.objects["roll"].SetCanBeMoved( True )

        self.hand = PhysObject()
        self.hand.Setup( ImageManager.Get( "hand" ), 0, 0, 1350, 156 )
        self.hand.SetPosition( 1280, 720 )
        self.hand.SetCrop( True )
        self.holding = "None"

        self.text = FontManager.Get( "main" ).render( self.steps[ self.step ], False, self.scoreColor )
        
        SoundManager.PlayMusic( "game" )
        
        self.gotoState = ""
        
    def Update( self ):
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if ( event.type == MOUSEBUTTONUP ):
                mousex, mousey = event.pos
                self.hand.SetupFrame( 0, 0, 1350, 156 )
                self.holding = "None"
                
                if ( self.step == 3 and self.objects["tp"].GetAngle() <= 95 and self.objects["tp"].GetAngle() >= 85 ):
                    self.step = 4

            elif ( event.type == MOUSEBUTTONDOWN ):
                self.hand.SetupFrame( 0, 156, 1350, 156 )

                for key, obj in self.objects.iteritems():
                    if ( self.IsBoxCollision( self.hand.posRect, obj.posRect ) ):
                        if ( self.holding == "None" ):
                            SoundManager.PlaySound( "pickup" )
                        self.holding = key

                        
                    if ( self.holding == key and key == "tp" and self.step == 3 ):                        
                        obj.Rotate( 5 )

            elif ( event.type == MOUSEMOTION ):
                mousex, mousey = event.pos
                self.hand.SetPosition( mousex - 70, mousey - 60 )

                for key, obj in self.objects.iteritems():
                    if ( self.holding == key and obj.GetCanBeMoved() == True ):
                        obj.SetPosition( mousex - 70, mousey - 60 )


            if ( event.type == KEYDOWN ):
                if ( event.key == K_d ):
                    print( "TP angle:", self.objects["tp"].GetAngle() )
                    print( "TP pos:", self.objects["tp"].posRect )
                    print( "Holder pos:", self.objects["tpholder"].posRect )
                    print( "Collision?", self.IsBoxCollision( self.objects["tp"].posRect, self.objects["tpholder"].posRect ) )

                elif ( event.key == K_ESCAPE ):
                    self.gotoState = "title"

        if ( self.step == 0 ):
            # Remove TP Roll
            if( self.objects["roll"].posRect.x != 180 and self.objects["roll"].posRect.y != 388 ):
                self.step = 1
                self.text = FontManager.Get( "main" ).render( self.steps[ self.step ], False, self.scoreColor )
                
        elif ( self.step == 1 ):
            # Throw away TP Roll
            if ( self.objects["roll"].posRect.x >= 260 and self.objects["roll"].posRect.x <= 290 and
                self.objects["roll"].posRect.y >= 580 ):
                    self.step = 2
                    self.objects["roll"].SetInactive( True )
                    self.text = FontManager.Get( "main" ).render( self.steps[ self.step ], False, self.scoreColor )
                    
        elif ( self.step == 2 ):
            # Put new TP on holder
            if ( self.objects["tp"].posRect.x >= 175 and self.objects["tp"].posRect.x <= 185 and
                self.objects["tp"].posRect.y >= 345 and self.objects["tp"].posRect.y <= 360 ):
                    self.step = 3
                    self.text = FontManager.Get( "main" ).render( self.steps[ self.step ], False, self.scoreColor )
                    self.objects["tp"].SetVelocity( 0, 0 )
                    self.objects["tp"].SetCanBeMoved( False )
        #elif ( self.step == 3 ):
            #print( self.objects["tp"].GetAngle() )
            #if ( self.objects["tp"].GetAngle() <= 90 and self.objects["tp"].GetAngle() >= 70 ):
                #self.step = 4
                #self.text = FontManager.Get( "main" ).render( self.steps[ self.step ], False, self.scoreColor )

        for key, obj in self.objects.iteritems():
            handrect = pygame.Rect( 0, 0, 150, 150 );
            handrect.x = self.hand.posRect.x
            handrect.y = self.hand.posRect.y
            if ( self.IsBoxCollision( handrect, obj.posRect ) ):
                if ( self.holding == key and obj.GetCanBeMoved() == True ):
                    continue
                if ( obj.GetIsInactive() == True ):
                    continue

                if ( key == "tp" and self.step == 3 ):
                    continue
                
                handMidY = self.hand.posRect.y + self.hand.posRect.height / 2
                tpMidY = obj.posRect.y + obj.posRect.height / 2
                
                handMidX = self.hand.posRect.x + 70
                tpMidX = obj.posRect.x + obj.posRect.width / 2
                
                if ( tpMidY < handMidY ):
                    obj.Accelerate( "UP" )
                    
                elif ( tpMidY > handMidY ):
                    obj.Accelerate( "DOWN" )
                    
                if ( tpMidX < handMidX ):
                    obj.Accelerate( "LEFT" )
                    
                elif ( tpMidX > handMidX ):
                    obj.Accelerate( "RIGHT" )

            obj.Update()

        #print( self.objects["tp"].xVel, self.objects["tp"].yVel )

        #if ( self.IsBoxCollision( self.objects["tp"].posRect, self.objects["tpholder"].posRect ) ):            
            #self.objects["tp"].SetVelocity( 0, 0 )

    
    
    def Draw( self, windowSurface ):
        windowSurface.fill( self.backgroundColor )
        windowSurface.blit( ImageManager.Get( "background" ), ( 0, 0 ) )

        windowSurface.blit( ImageManager.Get( "basketb" ), ( 224, 548 ) )

        self.objects["shelf"].Draw( windowSurface )
        
        self.objects["tpholder"].Draw( windowSurface )
        self.objects["roll"].Draw( windowSurface )

        self.objects["tp"].Draw( windowSurface )

        windowSurface.blit( ImageManager.Get( "basketf" ), ( 224, 548 ) )

        self.hand.Draw( windowSurface )
        
        if ( self.step < 4 ):
            windowSurface.blit( self.text, ( 10, 10 ) )
        elif ( self.step == 4 ):
            x = 300
            y = 720/2 - 60
            self.jitter = random.randint( -2, 2 )
            
            text1 = FontManager.Get( "win" ).render( "OH MY GOD, YOU DID IT!", False, pygame.Color( 255, 255, 0 ) )
            text2 = FontManager.Get( "win" ).render( "I THOUGHT IT WAS IMPOSSIBLE!!!", False, pygame.Color( 255, 0, 0 ) )
            windowSurface.blit( text1, ( x + self.jitter, y +  self.jitter ) )
            windowSurface.blit( text2, ( x - 100 + self.jitter, y + 60 + self.jitter ) )
    
