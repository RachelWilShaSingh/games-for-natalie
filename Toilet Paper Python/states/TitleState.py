#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math

import pygame, sys
from pygame.locals import *

from Ezha import BaseState
from Ezha import FontManager
from Ezha import ImageManager
from Ezha import LanguageManager
from Ezha import SoundManager
from Ezha import Button
from Ezha import Image
from Ezha import Label

from PhysObject import PhysObject

class TitleState( BaseState ):
    def __init__( self ):
        self.stateName = "titlestate"
        self.backgroundColor = pygame.Color( 151, 255, 140 )
        self.scoreColor = pygame.Color( 255, 255, 255 )


    def GotoState( self ):
        return self.gotoState
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight

        ImageManager.Add( "background",     "assets/background2.png" )
        ImageManager.Add( "button",         "assets/button.png" )
        ImageManager.Add( "toiletpaper",    "assets/fresh-tp.png" )
        ImageManager.Add( "title",          "assets/title.png" )

        self.animateCounter = 0;
        self.zoomCounter = 1;
        
        self.gotoState = ""
        
        self.buttonItems = {}
        
        self.imageItems = {}
        
        self.labelItems = {}
        
        self.imageItems["title"] = Image()
        self.imageItems["title"].Setup( { 
            "image" : ImageManager.Get( "title" ),
            "imagePosition" : ( 270, 50 ),
            "imageBlitRect" : ( 0, 0, 755, 173 ),
            "effect" : "bob"
            } )
        
        self.buttonItems["playButton"] = Button()
        self.buttonItems["playButton"].Setup( { 
            "backgroundImage" : ImageManager.Get( "button" ),
            "backgroundPosition" : ( 800, 300 ),
            #"iconImage" : self.images["button_icons"],
            #"iconPosition" : ( 40, 25 ),
            #"iconBlitRect" : ( 150, 0, 50, 50 ),
            "text" : "Play",
            "textPosition" : ( 110, 5 ),
            "textColor" : pygame.Color( 0, 0, 0 ),
            "font" : FontManager.Get( "main" )
            } )
        
        self.buttonItems["quitButton"] = Button()
        self.buttonItems["quitButton"].Setup( { 
            "backgroundImage" : ImageManager.Get( "button" ),
            "backgroundPosition" : ( 800, 500 ),
            #"iconImage" : self.images["button_icons"],
            #"iconPosition" : ( 40, 25 ),
            #"iconBlitRect" : ( 150, 0, 50, 50 ),
            "text" : "Exit",
            "textPosition" : ( 110, 5 ),
            "textColor" : pygame.Color( 0, 0, 0 ),
            "font" : FontManager.Get( "main" )
            } )

        self.labelItems["credit1"] = Label()
        self.labelItems["credit1"].Setup( {
            "font" : FontManager.Get( "credit" ),
            "text" : "Music from Incompetech, by Kevin MacLeod: Salty Ditty and Cyborg Ninja",
            "position" : ( 10, 720-30 ),
            "color" : pygame.Color( 255, 255, 255 ),
            "style" : "outline",
            "secondary_color" : pygame.Color( 0, 0, 0 )
            } )

        self.labelItems["credit2"] = Label()
        self.labelItems["credit2"].Setup( {
            "font" : FontManager.Get( "credit" ),
            "text" : "Microgame by Rachel Morris",
            "position" : ( 10, 720-60 ),
            "color" : pygame.Color( 255, 255, 255 ),
            "style" : "outline",
            "secondary_color" : pygame.Color( 0, 0, 0 )
            } )

        SoundManager.PlayMusic( "title" )
        
    def Update( self ):
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if ( event.type == MOUSEBUTTONUP ):
                mousex, mousey = event.pos

                if ( self.buttonItems["quitButton"].IsClicked( mousex, mousey ) == True ):
                    pygame.quit()
                    sys.exit()
                    
                elif ( self.buttonItems["playButton"].IsClicked( mousex, mousey ) == True ):
                    self.gotoState = "game"

        self.animateCounter = self.animateCounter + 1
        if ( self.animateCounter < 10 ):
            self.zoomCounter = self.zoomCounter + 0.1
            if ( self.zoomCounter > 10 ):
                self.zoomCounter = 10
            
        elif ( self.animateCounter < 20 ):
            self.zoomCounter = self.zoomCounter - 0.1
            if ( self.zoomCounter < 2 ):
                self.zoomCounter = 2
        else:
            self.animateCounter = 0
                


    
    
    def Draw( self, windowSurface ):
        windowSurface.fill( self.backgroundColor )
        windowSurface.blit( ImageManager.Get( "background" ), ( 0, 0 ) )
        
        for image in self.imageItems:
            self.imageItems[image].Draw( windowSurface )
            
        for button in self.buttonItems:
            self.buttonItems[button].Draw( windowSurface )
            
        for label in self.labelItems:
            self.labelItems[label].Draw( windowSurface )
        
        #text1 = FontManager.Get( "win" ).render( "TOILET PAPER: THE GAME", False, pygame.Color( 255, 255, 0 ) )
        #windowSurface.blit( text1, ( 250, 100 ) )

        img = ImageManager.Get( "toiletpaper" )
        rect = img.get_rect()
        img = pygame.transform.scale( img, (int(rect.width*self.zoomCounter), int(rect.height*self.zoomCounter) ) )
        rect = img.get_rect()
        windowSurface.blit( img, ( 350 - rect.width/2, 450 - rect.height/2 ) )
    
